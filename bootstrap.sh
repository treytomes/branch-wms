if [ ! -d "./node_modules" ]
then
  npm install
fi

if [ ! -d "./frontend/node_modules" ]
then
  cd frontend
  npm install
  cd ..
fi

if [ ! -d "./frontend-cdk/node_modules" ]
then
  cd frontend-cdk
  npm install
  cd ..
fi

if [ ! -d "./network/node_modules" ]
then
  cd network
  npm install
  cd ..
fi

if [ ! -d "./user-auth/node_modules" ]
then
  cd user-auth
  npm install
  cd ..
fi
